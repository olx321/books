export function insertItem(array, action) {
  let newArray = array.slice();
  newArray.splice(action.index, 0, action);
  return newArray;
}

export function removeItem(array, action) {
  let newArray = array.slice();
  newArray.splice(action.index, 1);
  return newArray;
}

export function getTags(tags = [], currentTag) {
  const newTags = tags;
  if (tags.includes(currentTag)) {
    return newTags;
  }

  newTags.push(currentTag);
  return newTags;
}
