import { createSelector } from 'reselect';

const getBooksToReadIds = (state) => state.books.toReadIds;
const getBooksInProgressIds = (state) => state.books.inProgressIds;
const getBooksDoneIds = (state) => state.books.doneIds;

const getTagsForToRead = (state) => state.filters.tags.toRead;
const getTagsForInProgress = (state) => state.filters.tags.inProgress;
const getTagsForDone = (state) => state.filters.tags.done;

const getAllBooks = (state) => state.books.byId;

const booksForToReadTabSelector = createSelector(
  [getBooksToReadIds, getAllBooks],
  (ids, allBooks) => ids.map((id) => allBooks[id]),
);
const booksForInProgressTabSelector = createSelector(
  [getBooksInProgressIds, getAllBooks],
  (ids, allBooks) => ids.map((id) => allBooks[id]),
);
const booksForDoneTabSelector = createSelector([getBooksDoneIds, getAllBooks], (ids, allBooks) =>
  ids.map((id) => allBooks[id]),
);

export const booksForToReadTabByTagSelector = createSelector(
  [booksForToReadTabSelector, getTagsForToRead],
  (booksByTabIds, tagsToFilter) => {
    return getBooksByTags(booksByTabIds, tagsToFilter);
  },
);
export const booksForInProgressTabByTagSelector = createSelector(
  [booksForInProgressTabSelector, getTagsForInProgress],
  (booksByTabIds, tagsToFilter) => {
    return getBooksByTags(booksByTabIds, tagsToFilter);
  },
);
export const booksForDoneTabByTagSelector = createSelector(
  [booksForDoneTabSelector, getTagsForDone],
  (booksByTabIds, tagsToFilter) => {
    return getBooksByTags(booksByTabIds, tagsToFilter);
  },
);

// export function selectBooksByTab(state, tabIds, tagsToFilter) {
//   const booksByTabIds = tabIds.map((id) => state.books.byId[id]);
//
//   if (!tagsToFilter || !tagsToFilter.length) {
//     return booksByTabIds;
//   }
//
//   const booksByTags = [];
//
//   for (let i = 0; i < booksByTabIds.length; i++) {
//     const currentBookTags = booksByTabIds[i].tags;
//
//     const intersection = tagsToFilter.filter((tag) => {
//       return currentBookTags.indexOf(tag) > -1;
//     });
//
//     if (intersection && intersection.length && tagsToFilter.length === intersection.length) {
//       booksByTags.push(booksByTabIds[i]);
//     }
//   }
//
//   return booksByTags;
// }

function getBooksByTags(booksByTabIds, tagsToFilter) {
  if (!tagsToFilter || !tagsToFilter.length) {
    return booksByTabIds;
  }

  const booksByTags = [];

  for (let i = 0; i < booksByTabIds.length; i++) {
    const currentBookTags = booksByTabIds[i].tags;

    const intersection = tagsToFilter.filter((tag) => {
      return currentBookTags.indexOf(tag) > -1;
    });

    if (intersection && intersection.length && tagsToFilter.length === intersection.length) {
      booksByTags.push(booksByTabIds[i]);
    }
  }

  return booksByTags;
}
