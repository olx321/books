import React, { PureComponent } from 'react';
import '../../App.css';
import { connect } from 'react-redux';
import Tag from '../../components/tag';

class BookItem extends PureComponent {
  render() {
    const { book } = this.props;

    return (
      <>
        {book.author && <div className="author">{book.author}</div>}
        <div className="item-wrap">
          {book.title && <div className="title">{book.title}</div>}
          {this.renderStatusButton()}
        </div>
        {book.description && <div className="description">{book.description}</div>}
        {book.tags && book.tags.length > 0 && (
          <div className="tags">
            {book.tags.map((tag, index) => (
              <Tag tag={tag} key={index} onTagClick={this.onTagClick} />
            ))}
          </div>
        )}
      </>
    );
  }

  renderStatusButton = () => {
    const { status, isLastItem } = this.props;

    const text =
      status === 'toRead'
        ? 'start reading →'
        : status === 'inProgress'
        ? 'finish reading →'
        : 'return in "to read"';
    return (
      <span className="action-link" onClick={() => this.onChangeBookStatusClick(isLastItem)}>
        {text}
      </span>
    );
  };

  onChangeBookStatusClick = (isLastItem) => {
    const { status, book, onChangeBookStatusClick, onClearTags } = this.props;

    onChangeBookStatusClick(book.id, status);

    if (isLastItem) {
      onClearTags(status);
    }
  };

  onTagClick = (tag) => {
    const { status, onTagClick } = this.props;
    onTagClick(tag, status);
  };
}

export default connect(
  () => ({}),
  (dispatch) => ({
    onTagClick: (tag, status) => {
      dispatch({ type: 'FILTER/BY_TAG', payload: { tag, status } });
    },
    onChangeBookStatusClick: (bookId, status) => {
      dispatch({ type: 'SET/BOOK_STATUS', payload: { bookId, status } });
    },
    onClearTags: (status) => {
      dispatch({ type: 'SET/CLEAR_TAGS_BY_TAB', payload: status });
    },
  }),
)(BookItem);
