import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import './App.css';
import './normalize.css';
import { Tabs } from 'antd';
import 'antd/dist/antd.css';
import BookItem from './containers/book';
import {
  booksForDoneTabByTagSelector,
  booksForInProgressTabByTagSelector,
  booksForToReadTabByTagSelector,
} from './selectors';
import Tag from './components/tag';
import { fetchBooks } from './index';

const { TabPane } = Tabs;

class App extends PureComponent {
  state = {
    // дефолтный таб
    activeTab: 'toRead',
  };

  componentDidMount() {
    this.props.fetchBooks();
  }

  render() {
    const { booksToRead, booksInProgress, booksDone, isLoading } = this.props;

    const showFiltersForBooksToRead =
      (booksToRead && booksToRead.length && this.state.activeTab === 'toRead') || isLoading;
    const showFiltersForBooksInProgress =
      (booksInProgress && booksInProgress.length && this.state.activeTab === 'inProgress') ||
      isLoading;
    const showFiltersForBooksDone =
      (booksDone && booksDone.length && this.state.activeTab === 'done') || isLoading;

    return (
      <div className="app-tabs">
        <Tabs
          onChange={this.onChangeTab}
          type="card"
          onTabClick={this.onTabClick}
          activeKey={this.state.activeTab}
        >
          <TabPane tab={`To read (${booksToRead.length || 0})`} key="toRead">
            {showFiltersForBooksToRead && this.renderFilterByTags('toRead')}
            {this.state.activeTab === 'toRead' && this.renderTab(booksToRead)}
          </TabPane>
          <TabPane tab={`In progress (${booksInProgress.length || 0})`} key="inProgress">
            {showFiltersForBooksInProgress && this.renderFilterByTags('inProgress')}
            {this.state.activeTab === 'inProgress' && this.renderTab(booksInProgress)}
          </TabPane>
          <TabPane tab={`Done (${booksDone.length || 0})`} key="done">
            {showFiltersForBooksDone && this.renderFilterByTags('done')}
            {this.state.activeTab === 'done' && this.renderTab(booksDone)}
          </TabPane>
        </Tabs>
      </div>
    );
  }

  onChangeTab = (activeKey) => {
    this.setState({
      activeTab: activeKey,
    });
  };

  renderTab = (books) => {
    const { isLoading, isError } = this.props;
    const { activeTab } = this.state;

    if (isError) {
      return <div className="list-error">Error fetching data</div>;
    }

    if (isLoading) {
      return <div className="list-loading">Loading...</div>;
    }

    if (!isLoading && books && books.length === 0) {
      return <div className="list-empty">List is empty</div>;
    }

    return (
      <ul className="list">
        {books.map((book, index) => (
          <li className="list-item" key={index}>
            <BookItem book={book} status={activeTab} isLastItem={books.length - 1 === index} />
          </li>
        ))}
      </ul>
    );
  };

  renderFilterByTags = (status) => {
    const { tags } = this.props;
    const { toRead, inProgress, done } = tags;

    if (status === 'toRead' && toRead && toRead.length) {
      return this.renderTags(toRead, 'toRead');
    }

    if (status === 'inProgress' && inProgress && inProgress.length) {
      return this.renderTags(inProgress, 'inProgress');
    }

    if (status === 'done' && done && done.length) {
      return this.renderTags(done, 'done');
    }
  };

  renderTags = (tags, status) => {
    return (
      <div className="tags-filter">
        <span className="tags-filter-title">Filtered by tags: </span>
        {tags.map((tag, index) => (
          <Tag tag={tag} key={index} onTagClick={this.onTagClick} />
        ))}
        <span className="clear-button" onClick={() => this.onClearClick(status)}>
          (clear)
        </span>
      </div>
    );
  };

  onTagClick = () => {
    return null;
  };

  onClearClick = (status) => {
    this.props.onClearTags(status);
  };
}

export default connect(
  (state) => ({
    booksToRead: booksForToReadTabByTagSelector(state),
    booksInProgress: booksForInProgressTabByTagSelector(state),
    booksDone: booksForDoneTabByTagSelector(state),
    // booksToRead: selectBooksByTab(state, state.books.toReadIds, state.filters.tags.toRead),
    // booksInProgress: selectBooksByTab( state, state.books.inProgressIds, state.filters.tags.inProgress),
    // booksDone: selectBooksByTab(state, state.books.doneIds, state.filters.tags.done),
    isLoading: state.books.isLoading,
    isError: state.books.isError,
    tags: state.filters.tags,
  }),
  (dispatch) => ({
    onClearTags: (status) => {
      dispatch({ type: 'SET/CLEAR_TAGS_BY_TAB', payload: status });
    },
    fetchBooks: () => {
      dispatch(fetchBooks());
    },
  }),
)(App);
