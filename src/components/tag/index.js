import React, { PureComponent } from 'react';
import '../../App.css';

class Tag extends PureComponent {
  render() {
    const { tag, onTagClick } = this.props;

    return (
      <span className="tag" onClick={() => onTagClick(tag)}>
        {`#${tag}`}
      </span>
    );
  }
}

export default Tag;
