import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import App from './App';
// import * as booksData from './data/10-items';
// import * as booksData30000 from './data/30000-items';
import { getTags, insertItem, removeItem } from './utils';
import thunk from 'redux-thunk';

const initialState = {
  books: {
    // все данные книг в сыром виде, отсортированные по id
    // при 30к книг такой способ хранения не подойдёт: будут тормоза
    // как вариант, данные в таком случае нужно получать чанками, например по 40 книг за раз и делать infinite scroll

    // вместо объекта объеков использовать массив объектов
    byId: {},
    // id всех книг
    // в целях оптимизации приложения от них можно избавиться, нужны для эмуляции данных пользователя в DEFAULT_STATE
    allIds: [],

    // массив id для каждого типа табов
    toReadIds: [],
    inProgressIds: [],
    doneIds: [],

    // статус получения данных
    isLoading: false,
    // ошибка загрузки данных
    isError: false,
  },
  filters: {
    tags: {
      // массивы тегов, по которым мы будем выводить книги при фильтрации, свой для каждого таба состояния
      toRead: [],
      inProgress: [],
      done: [],
    },
  },
};

function books(state = initialState, action) {
  console.log('action ', action);
  switch (action.type) {
    case 'FETCH/BOOKS':
      return {
        ...state,
        books: {
          ...state.books,
          isLoading: true,
          isError: false,
        },
      };

    case 'FETCH/BOOKS_ERROR':
      return {
        ...state,
        books: {
          ...state.books,
          isLoading: false,
          isError: true,
        },
      };

    case 'FETCH/BOOKS_SUCCESS':
      let normalizedBooksById = {};
      for (let i = 0; i < action.payload.length; i++) {
        let c = action.payload[i];
        normalizedBooksById[c.id] = c;
      }

      return {
        ...state,
        books: {
          ...state.books,
          byId: { ...state.byId, ...normalizedBooksById },
          allIds: action.payload.map((elem) => {
            return elem.id;
          }),
          toReadIds: action.payload.map((elem) => {
            return elem.id;
          }),
          isLoading: false,
          isError: false,
        },
      };

    // для создания фейкового стейта пользователя
    case 'SET/DEFAULT_STATE':
      const defaultBooksToRead = state.books.allIds.slice(0, state.books.allIds.length - 2);
      const defaultBooksInProgress = [state.books.allIds[state.books.allIds.length - 2]];
      const defaultBooksDone = [state.books.allIds[state.books.allIds.length - 1]];
      return {
        ...state,
        books: {
          ...state.books,
          toReadIds: defaultBooksToRead,
          inProgressIds: defaultBooksInProgress,
          doneIds: defaultBooksDone,
          isLoading: false,
          isError: false,
        },
        filters: {
          ...state.filters,
          tags: {
            toRead: ['boring'],
            inProgress: ['easy'],
            done: ['peasy'],
          },
        },
      };

    // при фильтрации по тегу/переносе книги по табам бывает кейс, когда одна и та же книга оказывается в двух табах
    case 'SET/BOOK_STATUS':
      let toReadIds = state.books.toReadIds;
      let inProgressIds = state.books.inProgressIds;
      let doneIds = state.books.doneIds;

      const status =
        action.payload.status === 'toRead'
          ? 'inProgress'
          : action.payload.status === 'inProgress'
          ? 'done'
          : 'toRead';

      if (status === 'inProgress') {
        toReadIds = removeItem(toReadIds, action.payload.bookId);
        inProgressIds = insertItem(inProgressIds, action.payload.bookId);
      }

      if (status === 'done') {
        inProgressIds = removeItem(inProgressIds, action.payload.bookId);
        doneIds = insertItem(doneIds, action.payload.bookId);
      }

      if (status === 'toRead') {
        doneIds = removeItem(doneIds, action.payload.bookId);
        toReadIds = insertItem(toReadIds, action.payload.bookId);
      }

      return {
        ...state,
        books: {
          ...state.books,
          toReadIds,
          inProgressIds,
          doneIds,
        },
      };

    case 'FILTER/BY_TAG':
      const newTags = getTags(
        Array.from(state.filters.tags[action.payload.status]),
        action.payload.tag,
      );
      const tagsToAdd = { ...state.filters.tags };
      tagsToAdd[action.payload.status] = newTags;

      return {
        ...state,
        filters: {
          ...state.filters,
          tags: tagsToAdd,
        },
      };

    case 'SET/CLEAR_TAGS_BY_TAB':
      const tags = { ...state.filters.tags };
      tags[action.payload] = [];

      return {
        ...state,
        filters: {
          ...state.filters,
          tags,
        },
      };

    default:
      return state;
  }
}

const receivedBooks = (json) => ({
  type: 'FETCH/BOOKS_SUCCESS',
  payload: json.items,
});

export function fetchBooks() {
  return (dispatch) => {
    dispatch({ type: 'FETCH/BOOKS' });
    return fetch('http://demo4174628.mockable.io/get10Books')
      .then(
        (response) => response.json(),
        // (reject) => console.log('Ошибка!', reject),
      )
      .then((json) => {
        dispatch(receivedBooks(json));
      })
      .catch((error) => {
        console.log('error ', error);
        dispatch({ type: 'FETCH/BOOKS_ERROR' });
      });
  };
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const enhancer = composeEnhancers(applyMiddleware(thunk));
const store = createStore(books, enhancer);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
);

store.subscribe(() => {
  console.log('subscribe ', store.getState());
});

// для работы с данными синхронно: раскомментировать 3 строки ниже, booksData и закомментировать fetchBooks() в App.js
// store.dispatch({ type: 'FETCH/BOOKS' });
// store.dispatch({ type: 'FETCH/BOOKS_SUCCESS', payload: booksData.items });
// store.dispatch({ type: 'SET/DEFAULT_STATE' });
